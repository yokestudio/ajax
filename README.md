# AJAX #

A JavaScript library for making AJAX calls.

Essentially syntactic sugar to make XMLHttpRequests:

* `request()` - generic request, with options
* `post()` - shortcut for request with method:POST
* `get()` - shortcut for request with method:GET
* `put()` - shortcut for request with method:PUT
* `delete()` - shortcut for request with method:DELETE
* `load()` - shortcut for request with method:GET, returnType:html. Only requires URL as param.

### Features ###
* CORS-enabled: automatically adds CORS-related headers if server URL is different from current location.
* Flexible enough to accept object literals, FormData or strings as data.
* Can use either callbacks or Promises (but not both).

### Browser Support ###
* IE 10+ (IE 9 support only with XMLHttpRequest polyfill)
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of 13 Jul 2016, v1.2.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="ajax-1.2.0.min.js"></script>`
    
3. Also consider including polyfills to normalize your target environments:
   [XMLHttpRequest (for IE 9)](https://github.com/Financial-Times/polyfill-service/blob/master/polyfills/XMLHttpRequest/polyfill.js),
   [ES6 Promises](https://github.com/taylorhakes/promise-polyfill),
   [ES-6 String includes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes#Polyfill)
### API ###
**`request`**
```
/**
 * @param: {object} settings
 *         {function} successCallback - optional,
 *         {function} failureCallback - optional.
 * @return: {Promise}
 */

// Sample usage (promises) - all settings enumerated
ajax.request({
	url: 'http://www.example.com/api.php', // if omitted, defaults to current location
	method: 'POST',                        // if omitted, defaults to 'GET'.
	headers: {'MY_TOKEN', 's0mEStR1nG'}    // custom headers that augment / overwrite the request headers. MUST be defined as object literal of string key-value pairs.
	crossDomain: true,                     // if omitted, will detect based on current window.location
	data: {name: "John Doe"},              // if omitted, will have no data sent to server. Accepts: [FormData object, strings or object literals].
	encType: 'form-data',                  // if omitted, will auto-detect. If specified, will ensure that data is converted to correct enctype (POST/PUT requests only).
	returnType: 'json',                    // if omitted, defaults to 'json'. ['text', 'html', 'xml', 'json']
	progressCallback: onProgress           // callback function on upload progress
}).then(function(result){                  // result is XMLHttpRequest object
	var statusCode = result.status;
	var dataJSON = JSON.parse(result.response);
	console.log('ajax success. Status: ' + statusCode);
	console.log(data);
}, function(result){                       // result is XMLHttpRequest object
	var statusCode = result.status;
	var errorJSON = JSON.parse(result.response);
	console.log('ajax error. Status: ' + statusCode);
	console.log(error_json);
});

/**
 * Note: `encType` is ignored for GET/DELETE requests.
 * Note: a POST/PUT request's encType must be one of: ['form-data', 'plain', 'urlencoded'].
 *       if auto-detected, depends on the data passed in.
 *       - {object literal}` --> `"application/x-www-form-urlencoded"`
 *       - {string}` --> `"text/plain"`
 *       - {FormData}` --> `"multipart/form-data"`
 */
```

**`post / put / get / delete`**
```
/**
 * @param: {object} settings
 *         {function} successCallback - optional,
 *         {function} failureCallback - optional.
 * @return: {Promise}
 */

// Sample usage (promises)
ajax.post({
	url: 'http://www.example.com/api.php'
	data:{"api":"test", "action":"testPost"},
	returnType:'json'
}).then(function(result){
	console.log(result);  //  result is XMLHttpRequest object
});
```

**`load`**
```
/**
 * @param: {string} url
 *         {function} successCallback - optional,
 *         {function} failureCallback - optional.
 * @return: {Promise}
 */

// Sample usage (callbacks)
ajax.load('http://www.example.com/new.html', function(result) {
	console.log('HTML of new page');
	console.log(result.response);
}, function(result) {
	var statusCode = result.status;
	var errorJSON = JSON.parse(result.response);
	console.log('ajax error. Status: ' + statusCode);
	console.log(error_json);
});
```
### FAQ ###

1. Why don't you simply use jQuery or other existing AJAX libraries?
> We have not found a library with all the features we need. Developers should not have to worry about CORS, form enctypes, custom headers etc., but most libraries we found require a bit of work to get these things working. We also prefer ES-6 compliant Promises, which jQuery didn't support.
>
> We designed this library to automatically detect sensible defaults so that if you don't specify the settings, things work as you'd expect them to. Refer to the API above.

1. Why doesn't it support synchronous AJAX calls?
> Synchronous AJAX is oxymoronic, and we believe it's a very bad practice to force AJAX calls to be synchronous. If you find a need for synchronous calls in your code, you have probably made a serious mistake in the design of your application.
>
>In other words, we won't support synchronous, blocking "AJAX" calls.

1. Does specifying returnType convert data after the AJAX call returns?
> No. It is only a request to the server to return data in a desired format. Setting returnType is equivalent to setting the HTTP Accept Header. You still have to make sure that the server is actually capable of returning data in that format type. 
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.