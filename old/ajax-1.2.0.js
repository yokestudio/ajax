/***
 * Library - AJAX
 * Syntactic sugar (jQuery-like) for making XMLHttpRequests to a server:
 *   - request()  (generic request, with options)
 *   - post()     (shortcut for request with method:POST)
 *   - get()      (shortcut for request with method:GET)
 *   - put()      (shortcut for request with method:PUT)
 *   - load()     (shortcut for request with method:GET, returnType:html settng, with only URL as param)
 *   - delete()   (shortcut for request with method:DELETE, returnType:html settng, with only URL as param)
 *
 * Features:
 *   - CORS-enabled: automatically adds CORS-related headers if url is different from current location.
 *   - Flexible enough to accept object literals, FormData or strings as data.
 *     Note: a POST/PUT request's enctype setting depend on the data passed in.
 *           {object literal} ==> "application/x-www-form-urlencoded"
 *           {string} ==> "text/plain"
 *           {FormData} ==> "multipart/form-data"
 *   - Can either use callbacks OR ES6-compliant promises, but not both.
 *
 * Usage example:
 *	ajax.request({
 *		url: 'http://www.example.com/api.php',       // if omitted, defaults to current location
 *		method: 'POST',                              // if omitted, defaults to 'GET'. ['POST', 'GET', 'PUT', 'DELETE']
 *      headers: {'CSRF_TOKEN', 's0mERaNdOmStR1nG'}  // custom headers that augment / overwrite the request headers. MUST be all string key-value pairs.
 *		crossDomain: true,                           // if omitted, will detect based on current window.location.host
 *		data: myFormData,                            // if omitted, will have no data sent to server. [FormData object, strings or object literals].
 *      encType: 'form-data',                        // if specified, will ensure that data is converted to correct enctype ['form-data', 'plain', 'urlencoded']. Note: ignored for GET/DELETE requests
 *		returnType: 'json',                          // if omitted, defaults to 'json'. ['text', 'html', 'xml', 'json']
 *      progressCallback: onProgress                 // callback function on upload progress
 *	}).then(function(result){                        // result is the XMLHttpRequest object
 *		var statusCode = result.status;
 *		var dataJSON = JSON.parse(result.response);
 *		console.log('ajax success ' + statusCode);
 *		console.log(data);
 *	}, function(result){                             // result is the XMLHttpRequest object
 *		var statusCode = result.status;
 *		var errorJSON = JSON.parse(result.response);
 *		console.log('ajax failed ' + statusCode);
 *		console.log(error_json);
 *	});
 ***/
(function(global) {
	'use strict';
	
	/**
	 * Makes AJAX request to server
	 * @param: {object} settings
	 *         {function} successCallback - optional,
	 *         {function} failureCallback - optional.
	 * @return: {Promise}
	 */
	function request(settings, successCallback, failureCallback) {
		// Check params
		if (typeof successCallback !== 'undefined') {
			if (typeof successCallback !== 'function') {
				throw new TypeError('ajax.request() - Expecting param2 to be success callback function. Got: ' + (typeof successCallback) + '. Value: ' + successCallback);
			}
			if (typeof failureCallback !== 'function') {
				throw new TypeError('ajax.request() - Expecting param3 to be failure callback function. Got: ' + (typeof failureCallback) + '. Value: ' + failureCallback);
			}
		} // using callbacks
		
		// Create a new request
		settings = processSettings(settings);
		var ajax_request = formAJAXRequest(settings);
		
		// Set up callbacks / promise
		if (typeof successCallback !== 'undefined') {
			return sendRequest(successCallback, failureCallback);
		}
		else {
			return new Promise(sendRequest);
		}
		
		function sendRequest(resolve, reject){
			// Listen for return from server
			ajax_request.onreadystatechange = function() {
				if (ajax_request.readyState !== 4) { // States: UNSENT == 0, OPENED == 1, HEADERS_RECEIVED == 2, LOADING == 3, DONE == 4
					return;	
				}
				
				// Check success or fail
				var status = ajax_request.status;
				if (status >= 200 && status < 300) { // HTTP Status: 200-series indicate success
					resolve(ajax_request);
				} // 200 series: success
				else {
					reject(ajax_request);
				} // non-200 series: fail
			};
			
			// Listen for network errors
			ajax_request.onerror = function() {
				reject(Error('Network Error'));
			};

			// Listen for progress events
			ajax_request.upload.onprogress = function(e) {
				if (e.lengthComputable) {
					if (settings.progressCallback) {
						settings.progressCallback(e.loaded, e.total);
					}
    			}
    			/* Not really an error...
    			else {
    				console.error('ajax sendRequest() - progress: e is not lengthComputable');
    			}
    			*/
			};
			
			// Send request after setting up listeners
			ajax_request.send(settings.data);
		} //sendRequest()
	} //request()
	
	/**
	 * POST/GET/PUT data to a server
	 * @param: settings (Object)
	 * @return: (Promise)
	 */
	function post(settings, successCallback, failureCallback) {
		settings.method = 'POST';
		return request(settings, successCallback, failureCallback);
	}	
	function get(settings, successCallback, failureCallback) {
		settings.method = 'GET';
		return request(settings, successCallback, failureCallback);
	}
	function put(settings, successCallback, failureCallback) {
		settings.method = 'PUT';
		return request(settings, successCallback, failureCallback);
	}
	function zdelete(settings, successCallback, failureCallback) {
		settings.method = 'DELETE';
		return request(settings, successCallback, failureCallback);
	}
	
	// Syntatic sugar for GET with returnType HTML
	function load(url, successCallback, failureCallback) {
		var settings = {
			url: url,
			method: 'GET',
			returnType:'html'
		};
		return request(settings, successCallback, failureCallback);
	}
	
	/**
	 * @private
	 * Ensures all settings are valid before making AJAX request to server
	 * - modifies settings object
	 * @param: settings (Object)
	 * @return: settings (Object)
	 */
	function processSettings(settings) {
		// Check URL
		if (typeof settings.url !== 'string') {
			settings.url = window.location.toString();
		}
		
		// Check method
		if (typeof settings.method !== 'string' || (settings.method !== 'GET' && settings.method !== 'POST' && settings.method !== 'PUT' && settings.method !== 'DELETE')) {
			settings.method = 'GET';
		}
		
		// Check for crossDomain
		if (typeof settings.crossDomain !== 'boolean') {
			settings.crossDomain = /^([\w-]+:)?\/\/([^\/]+)/.test(settings.url) && RegExp.$2 !== window.location.host;
		}
		
		// Check enctype (force-set to none for GET / DELETE)
		if (settings.method === 'GET' || settings.method === 'DELETE') {
			settings.encType = '';
		}
		else {
			if (settings.encType !== 'form-data' && settings.encType !== 'plain' && settings.encType !== 'urlencoded') {
				// Auto-detect if enctype is not set
				if (settings.data instanceof FormData) {
					settings.encType = 'form-data';
				}
				else if (typeof settings.data === 'string') {
					settings.encType = 'plain';
				}
				else {
					settings.encType = 'urlencoded';
				}
			} // enctype not set
		} // POST / PUT
		
		// Check data (convert to the specified encType)
		if (settings.encType === 'form-data' && !(settings.data instanceof FormData)) {
			var form_data = new FormData();
			
			// Convert data into FormData object
			if (typeof settings.data === 'object') {
				// Convert objects into FormData
				for (var key in settings.data) {
					if (settings.data.hasOwnProperty(key)) {
						form_data.append(key, settings.data[key]);
					}
				}
				settings.data = form_data;
			} 
			else {
				console.warn('ajax.processSettings() - for encType: form-data, expect data to be object, got :' + typeof settings.data + ' instead.');
				delete settings.data;
			}
		} // encType: form-data
		else if (settings.encType === 'plain' && typeof settings.data !== 'string') {
			// Convert non-strings into plain strings
			settings.data = JSON.stringify(settings.data);
		} // encType: plain
		else if ((settings.encType === 'urlencoded' || settings.encType === '') && typeof settings.data !== 'string') {
			if (typeof settings.data === 'object') {
				settings.data = object2urlencodedString(settings.data);
			}
			else if (typeof settings.data !== 'undefined') {
				console.warn('ajax.processSettings() - for encType: urlencoded || none, expect data to be string/object, got :' + typeof settings.data + ' instead.');
				delete settings.data;
			}
		} // encType: 'urlencoded' || ''
		
		// Check returnType (default: json)
		if (typeof settings.returnType !== 'string' || (settings.returnType !== 'text' && settings.returnType !== 'html' && settings.returnType !== 'xml')) {
			settings.returnType = 'json';
		}
		
		return settings;
	} //processSettings()
	
	/**
	 * @private
	 * Converts an object into a urlencoded strings (recursively)
	 */
	function object2urlencodedString(obj) {
		var queries = [];
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				if (typeof obj[key] === 'object') {
					queries.push(encodeURIComponent(key) + '=' + encodeURIComponent(JSON.stringify(obj[key])));
				}
				else {
					queries.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
				}
			}
		}
		return queries.join('&').replace('%20', '+');
	} //object2urlencodedString()
	
	/**
	 * @private
	 * Prepare an XMLHttpRequest based on the settings
	 * @param: {Object} settings
	 * @return: {XMLHttpRequest} request with settings
	 */
	function formAJAXRequest(settings) {
		var request = new XMLHttpRequest();
		if (settings.method === 'GET' && typeof settings.data !== 'undefined') {
			var separator = '?';
			if (settings.url.includes('?')) {
				separator = '&';
			}
			
			request.open('GET', settings.url+ separator +settings.data, true);
		}
		else {
			request.open(settings.method, settings.url, true);
		}
		
		// Prepare settings for CORS
		if (settings.crossDomain) {
			request.withCredentials = true;
		}
		else {
			request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		}
		
		// Set request content-type based on data encTypes
		if (settings.encType === 'plain') {
			request.setRequestHeader('Content-Type', 'text/plain; charset=UTF-8');
		}
		else if (settings.encType === 'urlencoded') {
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		}
		
		// Set desired return data type
		if (settings.returnType === 'text') {
			request.setRequestHeader('Accept', 'text/plain, */*; q=0.01');
		}
		else if (settings.returnType === 'html') {
			request.setRequestHeader('Accept', 'text/html, */*; q=0.01');
		}
		else if (settings.returnType === 'xml') {
			request.setRequestHeader('Accept', 'application/xml, text/xml, */*; q=0.01');
		}
		else {
			request.setRequestHeader('Accept', 'application/json, text/javascript, */*; q=0.01');
		}
		
		// Add custom headers
		for (var key in settings.headers) {
			if (settings.headers.hasOwnProperty(key) && typeof settings.headers[key] === 'string') {
				request.setRequestHeader(key, settings.headers[key]);
			}
		}
		
		return request;
	} //formAJAXRequest()
	
	global.ajax = {
		'request': request,
		'post': post,
		'get': get,
		'put': put,
		'delete': zdelete,
		'load': load
	};
})(this || {});