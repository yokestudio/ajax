/***
 * Library - AJAX
 * Syntactic sugar (jQuery-like) for making AJAX requests to a back end:
 *   - request()  (generic request, with options)
 *   - post()     (shortcut for request with method:POST and return type "json")
 *   - get()      (shortcut for request with method:GET and return type "json")
 *   - put()      (shortcut for request with method:PUT and return type "json")
 *   - load()     (shortcut for request with method:GET, with "html" return type, with only URL as param)
 *   - delete()   (shortcut for request with method:DELETE, returnType:html settng, with only URL as param)
 *
 * Description / Motivation:
 *   - Reduce boilerplate:
 *       - Maintain default settings that only need to be set once in the application lifetime.
 *         e.g. The desired data format is usually JSON. We can safely set Accept Header to "application/json".
 *         e.g. Cookies are not sent by default. We can safely set it to be sent.
 *         e.g. CORS is needed for back ends hosted on different domain. We automatically detect and add CORS-related headers.
 *
 *   - Fetch does not allow GET requests to have a body, as the payload are supposed to be URL parameters.
 *     This library allows any "body" that is object literal or FormData, and will automatically encode the "body" payload into URL params.
 *     Note: In older browsers, GET requests can only accept object literals (to be urlencoded), as FormData cannot be iterated.
 *
 *   - Fetch returns a Promise that resolves without data. Need to resolve simply to get the return data.
 *     We want to get a Promise that resolves with status and data, e.g.:
 *       {
 *         "status": 200,
 *         "data": {
 *           "message": "hello world!"
 *         }
 *       }
 *
 *       {
 *         "status": 404,
 *         "data": {
 *           "error": "world was not found."
 *         }
 *       }
 *
 * Usage example:
 *  ajax.request('http://www.example.com/api.php', {
 *      method: 'POST',                              // if omitted, defaults to 'GET'.    ['POST', 'GET', 'PUT', 'DELETE']
 *      headers: {'SOME_TOKEN', 's0mERaNdOmStR1nG'}  // custom headers that augment / overwrite default request headers.
 *      body: myFormData                             // if omitted, no data will be sent. [FormData object, strings or object literals].
 *  }).then(function(result){                        // result is an object with "status" and "data"
 *      var statusCode = result['status'];
 *      var data = result['data'];
 *      console.log('Server returned HTTP code: ' + statusCode);
 *      console.log(data);
 *  }, function(error) {                             // result is an Error object
 *      console.error(error);
 *  });
 ***/
(function(global) {
	'use strict';

	var defaults = {
		'method': 'GET',
		'headers': {
			'Accept': 'application/json'
		},
		'credentials': 'include',
		'mode': 'cors'
	};

	function setDefaults(settings) {
		defaults = settings;
	} // setDefaults()

	/**
	 * Makes AJAX request to server
	 * @param {string} url
	 * @param {object} user_settings
	 * @return {Promise} resolves with object containing status and data
	 */
	function request(url, user_settings) {
		if (typeof url !== 'string') {
			throw new TypeError('ajax.request() - expects URL to be string, got ' + typeof url);
		}

		var settings = processSettings(url, user_settings);
		var url_to_fetch = url;

		// Process body of requests
		if (settings['method'] === 'GET') {
			if (settings['body']) {
				if (typeof settings['body'] !== 'object') {
					throw new TypeError('ajax.request() - body must be object. Got ' + typeof settings['body']);
				}

				if (settings['body'] instanceof FormData) {
					if (typeof settings['body'].entries !== 'function') {
						throw new RangeError('ajax.request() - body for GET must be object literal in browsers without iterator support.');
					}

					// Convert to url encoded string
					var obj = {};
					var iterator = settings['body'].entries();
					var pair = iterator.next().value;
					while (pair && !pair.done) {
						obj[pair[0]] = pair[1];
						pair = iterator.next().value;
					}
					url_to_fetch += '?' + object2urlencodedString(obj);
				} // FormData object
				else {
					url_to_fetch += '?' + object2urlencodedString(settings['body']);
				} // object literal

				delete settings['body'];
			} // body exists
		} // GET request (must use URL encoded strings)
		else {
			if (settings['body']) {
				if (typeof settings['body'] !== 'object') {
					throw new TypeError('ajax.request() - body must be object. Got ' + typeof settings['body']);
				}

				if (!(settings['body'] instanceof FormData)) {
					// Convert objects into FormData
					var form_data = new FormData();
					for (var key in settings['body']) {
						if (settings['body'].hasOwnProperty(key)) {
							form_data.append(key, settings['body'][key]);
						}
					}
					settings['body'] = form_data;
				} // object literal
			} // body exists
		} // other requests (use FormData)

		// Make a Fetch!
		var response_status;
		return fetch(url_to_fetch, settings)
		.then(function(response) {
			response_status = response['status'];
			return response.text(); // return as text and parse to json later ourselves (don't trust response HTTP headers)
		})
		.then(function(text) {
			var data = text;
			if (settings['headers']['Accept'] === 'application/json') {
				try {
					if (text.length === 0) {
						data = undefined;
					}
					else {
						data = JSON.parse(text);
					}
				}
				catch (exception) {
					console.warn('ajax.request() - unable to parse response data to JSON. length: ' + text.length + '.');
				}
			}

			return {
				'status': response_status,
				'data': data
			};
		})
		.catch(function(error) {
			console.error(error);
			if (error.message) {
				throw new Error('Fetch failed while loading ' + url_to_fetch + '. Error message:' + error.message);
			}
			else {
				throw new Error('Fetch failed while loading ' + url_to_fetch + '. Error:' + error);
			}
		});
	} // request()

	/**
	 * Shortcut methods to POST/GET/PUT data to a server
	 * @param {string} url
	 *        {Object=} data
	 * @return {Promise} resolves with object containing status and data
	 */
	function ajax_post(url, data) {
		return request(url, {
			'method': 'POST',
			'body': data
		});
	}
	function ajax_get(url, data) {
		return request(url, {
			'method': 'GET',
			'body': data
		});
	}
	function ajax_put(url, data) {
		return request(url, {
			'method': 'PUT',
			'body': data
		});
	}
	function ajax_delete(url, data) {
		return request(url, {
			'method': 'DELETE',
			'body': data
		});
	}

	// Syntatic sugar for GET with returnType text
	function load(url) {
		var settings = {};
		settings['method'] = 'GET';
		settings['credentials'] = 'omit';
		settings['headers'] = {'Accept': '*/*'};
		return request(url, settings);
	}

	/**
	 * @private
	 * @param {string} url
	 * @param {Object} settings
	 * @return {Object}
	 * Apply defaults to settings. Derives some useful ones.
	 */
	function processSettings(url, settings) {
		var processed_settings = Object.assign({}, defaults, settings);

		if (typeof settings['headers'] === 'object') {
			if (settings['headers'] instanceof Headers) {
				// Convert to object
				var obj = {};
				var iterator = settings['headers'].entries();
				var pair = iterator.next().value;
				while (pair && !pair.done) {
					obj[pair[0]] = pair[1];
					pair = iterator.next().value;
				}

				processed_settings['headers'] = Object.assign({}, defaults['headers'], obj);
			}
			else {
				processed_settings['headers'] = Object.assign({}, defaults['headers'], settings['headers']);
			}
		}

		// Set as CORS if necessary
		if ((/^([\w-]+:)?\/\/([^/]+)/).test(url) && RegExp.$2 !== window.location.host) {
			processed_settings['mode'] = 'cors';
		} // URL is not origin

		// console.debug(JSON.stringify(processed_settings));

		return processed_settings;
	} // processSettings()

	/**
	 * @private
	 * @param {Object} obj
	 * @return {string}
	 * Converts an object into a urlencoded string
	 */
	function object2urlencodedString(obj) {
		var queries = [];
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				if (typeof obj[key] === 'object') {
					queries.push(encodeURIComponent(key) + '=' + encodeURIComponent(JSON.stringify(obj[key])));
				}
				else {
					queries.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
				}
			}
		}

		// console.debug(queries);
		return queries.join('&').replace('%20', '+');
	} // object2urlencodedString()

	global['ajax'] = {
		'setDefaults': setDefaults,
		'request': request,
		'post': ajax_post,
		'get': ajax_get,
		'put': ajax_put,
		'delete': ajax_delete,
		'load': load
	};
}(this || {}));
